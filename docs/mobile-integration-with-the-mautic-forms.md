## Mobile integration with the Mautic Forms

Mautic Forms are submitted using a public endpoint - `/form/submit`.

`/form/submit` endpoint accepts a POST request including `urlencoded` form fields. Cookies should also be specified to allow mautic to merge contacts whenever it discovers new data from unique identifiers.

In order to simplify integration for mobile applications, cookies persistence and http/s protocol implementation (example: Set-Cookie header should automatically set cookies) is an important matter.

## Request

### The URL parameters

To get a response body in JSON format, `ajax` parameter should be assigned with `1` value.

```
/form/submit?ajax=1
```

### Headers

Pay attention to the `Content-Type` and `Accept` headers in the request. A request should include `Content-Type: application/x-www-form-urlencoded` and `Accept: application/json` headers.

### Cookies

Visitors are identified by mautic tracking cookies. If no cookies exist yet, Mautic will create a new anonymous contact at first time public endpoint `/mtc/events` is invoked and corresponding cookies will be returned in the response. 

The tracking cookies:

- `mautic_device_id` -- holds Mautic session ID defined by PHP
- `mautic_session_id` -- holds Mautic session ID defined by PHP
- `mtc_sid` -- holds Mautic session ID defined by JS
- `mtc_id` -- holds Mautic Contact ID defined by JS

Cookies above should always be included in the request header (that's what allows mautic to merge contact being monitored at the moment with an existing contact corresponding to the submitted data or update monitored contact with submitted data).

Example:
```
mtc_id=2;
mautic_device_id=zvlkvyu4gmsdm5n1f6iuqku;
mtc_sid=zvlkvyu4gmsdm5n1f6iuqku;
mautic_session_id=zvlkvyu4gmsdm5n1f6iuqku;
zvlkvyu4gmsdm5n1f6iuqku=2
```

### Body

Request body should include an array with `mauticform` key containing the following fields:

- `formId` (int) (required) -- a corresponding form ID;

- *alias of the custom field(s) that was bound to a form field.

Example:
```
mauticform[formId]:1                            // formId
mauticform[email]:mark.blacker@codefive.pt      // alias: email
mauticform[points]:100                          // alias: points
```

## Response

If a form submission request is successful and the monitored contact is merged with an existing one (because submitted form data allowed mautic to identify that contact), new cookies (Set-Cookie headers) will be received regarding the matched contact.
If a form submission request is successful and the monitored contact is only updated with submitted form data, no new cookies will be received in the response (as monitored mautic contact remains the same) with exception to .

As a result for your request you will get `200` status and the JSON body with the `success` attribute which indicates whether a specific HTTP request has been successfully completed.

A successful response is when you will get the `200 OK` status code AND the `"success":1` attribute of a JSON body.

## Examples

An example of a request (`"success":1`) - monitored contact changed because the submitted data allowed contact identification resulting on Set-Cookie headers in the response
```http
POST /form/submit?ajax=1 HTTP/1.1
Accept: application/json
User-Agent: PostmanRuntime/7.26.1
Postman-Token: 1fdd3b28-d55d-48d5-8141-c6bf0a7dc58b
Host: 192.168.0.108:8081
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Type: application/x-www-form-urlencoded
Content-Length: 100
Cookie: f3fdad43054b85e090478a8ee7b8df63=7666889142342f741fbf968aaeab6711; mautic_device_id=0z5uk4mtcqeo2yxacyu1e2a; mtc_id=71; mtc_sid=0z5uk4mtcqeo2yxacyu1e2a; mautic_session_id=0z5uk4mtcqeo2yxacyu1e2a; 0z5uk4mtcqeo2yxacyu1e2a=71

mauticform%5Bemail%5D=mark.blacker%40codefive.pt&mauticform%5Bpoints%5D=100&mauticform%5BformId%5D=1
```

An example of a response (`"success":1`)
```http
HTTP/1.1 200 OK
Date: Tue, 07 Jul 2020 09:41:58 GMT
Server: Apache/2.4.38 (Debian)
X-Powered-By: PHP/7.1.33
Set-Cookie: f3fdad43054b85e090478a8ee7b8df63=068782dfb7b0e75e0f5bb5b52bb4e94d; path=/; HttpOnly
Set-Cookie: mautic_session_id=deleted; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0; path=/
Set-Cookie: mautic_device_id=zvlkvyu4gmsdm5n1f6iuqku; expires=Wed, 07-Jul-2021 09:41:59 GMT; Max-Age=31536000; path=/
Set-Cookie: mtc_id=2; path=/
Set-Cookie: mtc_sid=zvlkvyu4gmsdm5n1f6iuqku; path=/
Set-Cookie: mautic_session_id=zvlkvyu4gmsdm5n1f6iuqku; expires=Wed, 07-Jul-2021 09:41:59 GMT; Max-Age=31536000; path=/
Set-Cookie: zvlkvyu4gmsdm5n1f6iuqku=2; expires=Wed, 07-Jul-2021 09:41:59 GMT; Max-Age=31536000; path=/
Cache-Control: no-cache
Content-Length: 73
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
Content-Type: application/json

{"success":1,"results":{"email":"mark.blacker@codefive.pt","points":100}}
```

An example of a request with cookies (`"success":0`)
```http
POST /form/submit?ajax=1 HTTP/1.1
Accept: application/json
User-Agent: PostmanRuntime/7.26.1
Postman-Token: 7e50e770-19f1-4306-9435-0a8343df157a
Host: localhost:8081
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Type: application/x-www-form-urlencoded
Content-Length: 100
Cookie: f3fdad43054b85e090478a8ee7b8df63=7666889142342f741fbf968aaeab6711; mautic_device_id=0z5uk4mtcqeo2yxacyu1e2a; mtc_id=71; mtc_sid=0z5uk4mtcqeo2yxacyu1e2a; mautic_session_id=0z5uk4mtcqeo2yxacyu1e2a; 0z5uk4mtcqeo2yxacyu1e2a=71

mauticform%5Bemail%5D=mark.blacker%40codefive.pt&mauticform%5Bpoints%5D=100&mauticform%5BformId%5D=1
```
An example of a response (`"success":1`)
```http
HTTP/1.1 200 OK
Date: Thu, 16 Jul 2020 08:07:11 GMT
Server: Apache/2.4.38 (Debian)
X-Powered-By: PHP/7.1.33
Cache-Control: no-cache
Content-Length: 73
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
Content-Type: application/json

{"success":1,"results":{"email":"mark.blacker@codefive.pt","points":100}}
```

An example of a request (`"success":0`)
```http
POST /form/submit?ajax=1 HTTP/1.1
Accept: application/json
User-Agent: PostmanRuntime/7.26.1
Postman-Token: a1566982-a76a-46ff-9231-27a1b3a07768
Host: 192.168.0.108:8081
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Type: application/x-www-form-urlencoded
Content-Length: 100

mauticform%5Bemail%5D=mark.blacker%40codefive.pt&mauticform%5Bpoints%5D=100&mauticform%5BformId%5D=0
```

An example of a response (`"success":0`)
```http
HTTP/1.1 200 OK
Date: Mon, 06 Jul 2020 16:20:30 GMT
Server: Apache/2.4.38 (Debian)
X-Powered-By: PHP/7.1.33
Set-Cookie: f3fdad43054b85e090478a8ee7b8df63=0bf0f9221d510862b4d6e94291f8db65; path=/; HttpOnly
Cache-Control: no-cache
Content-Length: 64
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
Content-Type: application/json

{"success":0,"errorMessage":"This form is no longer available."}
```

An example of a request with the required `points` field (`"success":0`)
```http
POST /form/submit?ajax=1 HTTP/1.1
Accept: application/json
User-Agent: PostmanRuntime/7.26.1
Postman-Token: 277821e2-19de-4fd5-a708-e71d7e418859
Host: 192.168.0.108:8081
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Type: application/x-www-form-urlencoded
Content-Length: 73

mauticform%5Bemail%5D=mark.blacker%40codefive.pt&mauticform%5BformId%5D=1
```

An example of a response with the required `points` field (`"success":0`)
```http
HTTP/1.1 200 OK
Date: Mon, 06 Jul 2020 16:25:44 GMT
Server: Apache/2.4.38 (Debian)
X-Powered-By: PHP/7.1.33
Set-Cookie: f3fdad43054b85e090478a8ee7b8df63=ebab6f7bf7d1b059e949bf1e999cacbe; path=/; HttpOnly
Cache-Control: no-cache
Content-Length: 77
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
Content-Type: application/json

{"success":0,"validationErrors":{"points":"\u0027Points\u0027 is required."}}
```