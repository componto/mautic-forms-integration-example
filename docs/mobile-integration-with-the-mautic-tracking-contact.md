# Using Mautic Tracking System in mobile apps context


Mobile apps should be abble to handle HTTP/S requests and cookies persistence.

Mautic automatically generates some data to identify tracked users when async javascript tracking code is triggered (on load, similar to google analytics tracking code) - Such data is generated after first time tracking code runs and information is sent to the public endpoint `/mtc/event`. Such information is saved as cookies and is what allows mautic tracking system to identify new/returning users in order to save behavioral information gathered in the monitored information systems to the correct mautic contacts.

Managing cookies in mobile apps can be done using a cookie manager (example: [CookieSyncManager](https://developer.android.com/reference/android/webkit/CookieSyncManager)) to synchronize browser cookie store between RAM and permanent storage.

Mautic tracking system can update contacts custom fields, but only fields set as `Publicly updatable` in mautic backoffice will allow such feature - security purposes. Nevertheless, note that mautic tracking system was designed to track behavioral events (page hits, for example) and should be used whenever that happens (website's mautic JS tracking code implement this automatically - every time a page loads mautic tracking system will add a page hit to the contact history, including client and page information). 

Endpoint `/mtc/event` accepts a `POST` request including `urlencoded` form fields. Excluding the very first request for a new user case, tracking cookies should also be included in the request.

Long story - short version: Every first visit to a mobile application should result in a request to `/mtc/event`, saving the resulting cookies (which come in Set-Cookie response headers). All the consequent requests (that is, every time the user enters a new screen/page in the app and `/mtc/event` is invoked again - stating page_title, page_url, etc etc - explained below) headers should include a Cookie header including all the cookies stored in the first request. If HTTP/S protocol is implemented (that is, Set-Cookie headers automatically result in saving that cookies and allow them to be used in the the following requests), everytime a form submission or any other actions that can change the monitored contact occur, such cookie changes will be automatically reflected (because any time monitored contact changes, Set-Cookie headers will be returned in the corresponding response).


> **Important**: Mobile apps developers should make sure that HTTP requests use necessary cookies. First request will get the necessary cookies in the response and such cookies should be reused from one request to the next. This is how Mautic (and other tracking softwares) checks if the user is the same within the requests. Otherwise, one may run into the (unlikely but possible) case where there will be multiple contacts from the same IP address and Mautic will end up merging them all into a single contact as it can't tell who is who without a cookie.

## Request

### Headers

Mobile apps developers should pay attention to `Content-Type` and `Accept` headers in the request. A request should have `Content-Type: application/x-www-form-urlencoded` and `Accept: application/json` headers.

Mautic uses the `piwik/device-detector` package that parses User Agents and detects devices (desktop, tablet, mobile, tv, cars, console, etc.), clients (browsers, media players, mobile apps, feed readers, libraries, etc), operating systems, devices, brands and models.

You need to provide the `User-Agent` header for each request.

Please see the link for more details [https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/User-Agent]

An example of a value for the `User-Agent` header
```
MyApplication/1.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X)
```


### Cookies

Visitors are identified only by mautic cookies. If no cookies exist yet, in the first request to public endpoint /mtc/events Mautic will create a new anonymous contact and will send its corresponding cookies in the request response. 

Mautic tracking cookies (after first request for a first time user, these cookies should be saved and sent in every request headers - for all public endpoints and REST API):

- `mautic_device_id` -- holds Mautic session ID (device_id) defined by PHP
- `mautic_session_id` -- holds Mautic session ID (device_id) defined by PHP
- `mtc_sid` -- holds Mautic session ID (device_id) defined by JS
- `mtc_id` -- holds Mautic Contact ID defined by JS

Specifying cookies is mandatory to be able to track (using mautic tracking system) a certain contact.

Example cookies:
```
mtc_id=2;
mautic_device_id=zvlkvyu4gmsdm5n1f6iuqku;
mtc_sid=zvlkvyu4gmsdm5n1f6iuqku;
mautic_session_id=zvlkvyu4gmsdm5n1f6iuqku;
zvlkvyu4gmsdm5n1f6iuqku=2
```

### Body

Mautic accepts `urlencoded` data containing the following fields (plus custom fields - explained below):

- `page_url` (required) -- URL of the current page or the name of a screen view (main_view);
- `page_title` (important) -- Data present between `<title></title>` tags or a provided title of a screen view;
- `page_language` (important) -- Browser language(application);
- `page_referrer` (important) -- URL which the contact came from to the current website. A name of a screen view before current one (order_view);
- `counter` (optional) -- Amount of visits regarding the current page;
- `mautic_device_id` (optional) -- A device_id which was saved in a cookie after first tracking submission.
- `resolution` -- a Width x Height of the device display resolution.
- `timezone_offset` (important) -- Amount of minutes plus or minus from UTC.
- `platform` -- The platform of the device. Usually OS and processor architecture.

An example for website (note: if not using mt javascript function dont forget that user agent should always be sent in http headers):
```
page_title: Website title
page_language: en-US
page_referrer: http://link_user_came.from
page_url: http://visited_page.url
resolution: 1334x750
timezone_offset: -60
platform: iOS
```

An example for a mobile application (note: user agent should always be sent in http headers):
```
page_title: My Screen Title
page_language: en-US
page_referrer: main_view
page_url: settings_view
counter: 0
resolution: 1334x750
timezone_offset: -60
platform: iOS
```

Mautic tracking system (or more specifically, public endpoint `/mtc/event`) can and should be used to update custom fields (only `Publicly updatable` custom fields). For security reasons, updating non-`Publicly updatable` custom fields can only be done using REST API (because it enforces authentication).

An example with custom fields
```
page_title: WP â€“ Just another WordPress site
page_language: en-US
page_referrer: http://link_user_came.from
page_url: http://visited_page.url
email: my@email.com                                 // the email alias of the custom field
firstname: John                                     // the firstname alias of the custom field
lastname: Wick                                      // the lastname alias of the custom field
resolution: 1920x1080
timezone_offset: -60
platform: Linux x86_64
```

## Response

### Status Code

`200 OK` status code will always be returned, except when server issues occur.

### Body

If the response is successful, response of the very first request to `/mtc/event` will include the Set-Cookie headers belonging to the contact being tracked (if it's a first request, it will be from an anonymous contact). Nevertheless, a form submission, for example, can change the contact being monitored - ex: if monitored contact is anonymous and an email of an existing lead is provided in a form submission, monitored contact will change to the existing lead and anonymous contact will be merged and form submission response will include new Set-Cookie headers), that's why is so important to make sure that Set-Cookie headers will always result in cookies update in frontend and that Cookie header (including current mautic tracking cookies) is present in every header of every requests made to all public mautic endpoints (and also some REST API endpoints).

Request responses will get a`200` status code and a JSON body with a `success` attribute which indicates whether a specific HTTP request has been successfully completed or not.

A successful response fulfils the following conditions: `200 OK` status code AND `"success":1` attribute in the response JSON body AND the attributes: `id`, `sid`, `device_id` not containing `null` value.

A successful response example:
```json
{
    "success": 1,
    "id": 21,
    "sid": "ur3f4gdu09auwajenmd0oj5",
    "device_id": "ur3f4gdu09auwajenmd0oj5",
    "events": [
        false
    ]
}
```

A failed response example:
```json
{
    "success": 1,
    "id": null,
    "sid": null,
    "device_id": null,
    "events": [
        false
    ]
}
```

A rejected request (non trackable device)
```json
{
    "success": 0
}
```

## Examples

An example of a request (very first request)
```http
POST /mtc/event HTTP/1.1
User-Agent: PostmanRuntime/7.26.1
Accept: */*
Postman-Token: 02f29ad7-4214-4b9d-993b-6ef497a610e8
Host: 172.164.245.123
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Type: application/x-www-form-urlencoded
Content-Length: 326

page_title=%20WP%20%E2%80%93%20Just%20another%20WordPress%20site&page_language=%20en-US&page_referrer=%20&page_url=%20http%3A%2F%2Flocalhost%3A8000%2F&resolution=%201920x1080&timezone_offset=%20-60&platform=%20Linux%20x86_64
```

An example of a response for a first request (unknown user - anonymous mautic contact cookies will be provided)
```http
HTTP/1.1 200 OK
Date: Wed, 15 Jul 2020 11:01:59 GMT
Server: Apache/2.4.38 (Debian)
X-Powered-By: PHP/7.1.33
Set-Cookie: f3fdad43054b85e090478a8ee7b8df63=bb947fff28b2c35c843d15b1e8c51571; path=/; HttpOnly
Set-Cookie: mautic_session_id=deleted; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0; path=/
Set-Cookie: mautic_device_id=ur3f4gdu09auwajenmd0oj5; expires=Thu, 15-Jul-2021 11:02:00 GMT; Max-Age=31536000; path=/
Set-Cookie: mtc_id=21; path=/
Set-Cookie: mtc_sid=ur3f4gdu09auwajenmd0oj5; path=/
Set-Cookie: mautic_session_id=ur3f4gdu09auwajenmd0oj5; expires=Thu, 15-Jul-2021 11:02:00 GMT; Max-Age=31536000; path=/
Set-Cookie: ur3f4gdu09auwajenmd0oj5=21; expires=Thu, 15-Jul-2021 11:02:00 GMT; Max-Age=31536000; path=/
Set-Cookie: mautic_referer_id=56; expires=Wed, 15-Jul-2020 11:32:00 GMT; Max-Age=1800; path=/
Cache-Control: no-cache
Content-Length: 108
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
Content-Type: application/json

{"success":1,"id":21,"sid":"ur3f4gdu09auwajenmd0oj5","device_id":"ur3f4gdu09auwajenmd0oj5","events":[false]}
```

An example of a request
```http
POST /mtc/event HTTP/1.1
User-Agent: PostmanRuntime/7.26.1
Accept: */*
Postman-Token: ec499546-b0f7-49f2-9e40-f9f237a2a07f
Host: 172.164.245.123
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Type: application/x-www-form-urlencoded
Content-Length: 385
Cookie: f3fdad43054b85e090478a8ee7b8df63=bb947fff28b2c35c843d15b1e8c51571; mautic_device_id=ur3f4gdu09auwajenmd0oj5; mtc_id=21; mtc_sid=ur3f4gdu09auwajenmd0oj5; mautic_session_id=ur3f4gdu09auwajenmd0oj5; ur3f4gdu09auwajenmd0oj5=21

page_title=%20WP%20%E2%80%93%20Just%20another%20WordPress%20site&page_language=%20en-US&page_referrer=%20&page_url=%20http%3A%2F%2Flocalhost%3A8000%2F&resolution=%201920x1080&timezone_offset=%20-60&platform=%20Linux%20x86_64&email=%20my%40email.com&firstname=%20John&lastname=%20Wick
```

An example of a response
```http
HTTP/1.1 200 OK
Date: Wed, 15 Jul 2020 13:40:46 GMT
Server: Apache/2.4.38 (Debian)
X-Powered-By: PHP/7.1.33
Set-Cookie: mautic_referer_id=60; expires=Wed, 15-Jul-2020 14:10:47 GMT; Max-Age=1800; path=/
Cache-Control: no-cache
Content-Length: 108
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
Content-Type: application/json

{"success":1,"id":21,"sid":"ur3f4gdu09auwajenmd0oj5","device_id":"ur3f4gdu09auwajenmd0oj5","events":[false]}
```

An example of a bad request (without cookies and data)
```http
POST /mtc/event HTTP/1.1
User-Agent: PostmanRuntime/7.26.1
Accept: */*
Postman-Token: 879bea77-3ae9-4adc-98c4-cf76fd35bb39
Host: 172.164.245.123
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 0
```

An example of a failed response
```http
HTTP/1.1 200 OK
Date: Wed, 15 Jul 2020 13:42:24 GMT
Server: Apache/2.4.38 (Debian)
X-Powered-By: PHP/7.1.33
Set-Cookie: f3fdad43054b85e090478a8ee7b8df63=71fadd877e5b3984641d3881391609d9; path=/; HttpOnly
Cache-Control: no-cache
Content-Length: 68
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
Content-Type: application/json

{"success":1,"id":null,"sid":null,"device_id":null,"events":[false]}
```