## Create contacts (batch) request

```http
POST /api/contacts/batch/new HTTP/1.1
```

### Restrictions

Mautic processes limited amount of items (contacts) per one request and also a limited execution time that a script is allowed to run (https://www.php.net/manual/en/info.configuration.php#ini.max-execution-time).

You can add the `api_batch_max_limit` parameter to your configuration `app/config/local.php` file as `'api_batch_max_limit' => 100`, then clear the cache.

`Default: 200`

## Request

### Headers

To authorize a request for basic authentication, set an `Authorization` header.

1. Combine the username and password of a Mautic user with a colon `:`. For example, `admin:password`.
2. Base64 encode the string from above. `YWRtaW46cGFzc3dvcmQ=`.
3. Add an Authorization header to each API request as `Authorization: Basic YWRtaW46cGFzc3dvcmQ=`

### Cookies

No cookies required.

### Body

You can send a body like `a JSON array`, for that you need to use the square brackets `[ ]` and your objects inside or you can send `a JSON object` that is written in key/value pairs and surrounded by curly brackets `{ }`.

An example of a JSON array
```json
[
    {
        "email": "email1@domain.com",
        "points": 10
    },
    {
        "email": "email2@domain.com",
        "points": 11
    },
    {
        "email": "email3@domain.com",
        "points": 12
    }
]
```

An example of a JSON object
```json
{
    "key-1": {
        "email": "email1@domain.com",
        "points": 10
    },
    "key-2": {
        "email": "email2@domain.com",
        "points": 11
    },
    "key-3": {
        "email": "email3@domain.com",
        "points": 12
    }
}
```

## Response

### Status Code

You will get always the `201 Created` status code, except server issues.

### Body

A JSON object of a response body contains the `contacts` and the `statusCodes` objects and if there are some errors you will get the `errors` object.

For each object (contact) you will get a status code which indicates a result of operation on it.

Status codes

* `200` OK -- updated contact;
* `201` Created -- created contact;
* `400` Bad Request -- rejected contact, because of errors that are listed at the `errors` object.

An example of response (`a JSON array`)

```json
{
    "contacts": [
        //
    ],
    "statusCodes": [
        201,
        201,
        201
    ]
}
```
An example of response (`a JSON object`)
```json
{
    "contacts": [
        //
    ],
    "statusCodes": {
        "key-1": 201,
        "key-2": 201,
        "key-3": 201
    }
}
```

An example of response with errors (`a JSON object`)
```json
{
    "contacts": [
        //
    ],
    "statusCodes": {
        "key-1": 200,
        "key-2": 200,
        "key-3": 200,
        "key-4": 400
    },
    "errors": {
        "key-4": {
            "code": 400,
            "message": "email: A valid email is required., points: This value is not valid.",
            "details": {
                "email": [
                    "A valid email is required."
                ],
                "points": [
                    "This value is not valid."
                ]
            },
            "type": null
        }
    }
}
```

## Examples

An example of request
```http
POST /api/contacts/batch/new HTTP/1.1
Authorization: Basic YWRtaW46cGFzc3dvcmQ=
Content-Type: application/json
User-Agent: PostmanRuntime/7.26.1
Accept: */*
Postman-Token: 75b35eb1-0443-4c91-80f4-08e51ef67861
Host: localhost:8081
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 218

[
    {
        "email": "email1@domain.com",
        "points": 10
    },
    {
        "email": "email2@domain.com",
        "points": 11
    },
    {
        "email": "email3@domain.com",
        "points": 12
    }
]
```

An example of response

```http
HTTP/1.1 201 Created
Date: Thu, 09 Jul 2020 16:14:13 GMT
Server: Apache/2.4.38 (Debian)
Vary: Authorization
X-Powered-By: PHP/7.1.33
Set-Cookie: f3fdad43054b85e090478a8ee7b8df63=fea268abd02af0a4ae30337a32dc3f37; path=/; HttpOnly
Mautic-Version: 2.16.2
Cache-Control: no-cache
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
Transfer-Encoding: chunked
Content-Type: application/json

{
    "contacts": [
        {
            "isPublished": true,
            "dateAdded": "2020-07-09T16:14:14+00:00",
            "dateModified": null,
            "createdBy": 1,
            "createdByUser": "Super Admin",
            "modifiedBy": null,
            "modifiedByUser": null,
            "id": 32,
            "points": 10,
            "color": null,
            "fields": {
                "core": {
                    "title": {
                        "id": 1,
                        "group": "core",
                        "label": "Title",
                        "alias": "title",
                        "type": "lookup",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "firstname": {
                        "id": 2,
                        "group": "core",
                        "label": "First Name",
                        "alias": "firstname",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "lastname": {
                        "id": 3,
                        "group": "core",
                        "label": "Last Name",
                        "alias": "lastname",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "company": {
                        "id": 4,
                        "group": "core",
                        "label": "Primary company",
                        "alias": "company",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "position": {
                        "id": 5,
                        "group": "core",
                        "label": "Position",
                        "alias": "position",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "email": {
                        "id": 6,
                        "group": "core",
                        "label": "Email",
                        "alias": "email",
                        "type": "email",
                        "object": "lead",
                        "value": "email1@domain.com",
                        "normalizedValue": "email1@domain.com"
                    },
                    "mobile": {
                        "id": 7,
                        "group": "core",
                        "label": "Mobile",
                        "alias": "mobile",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "phone": {
                        "id": 8,
                        "group": "core",
                        "label": "Phone",
                        "alias": "phone",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "points": {
                        "id": 9,
                        "group": "core",
                        "label": "Points",
                        "alias": "points",
                        "type": "number",
                        "object": "lead",
                        "value": 10,
                        "normalizedValue": 10
                    },
                    "fax": {
                        "id": 10,
                        "group": "core",
                        "label": "Fax",
                        "alias": "fax",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "address1": {
                        "id": 11,
                        "group": "core",
                        "label": "Address Line 1",
                        "alias": "address1",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "address2": {
                        "id": 12,
                        "group": "core",
                        "label": "Address Line 2",
                        "alias": "address2",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "city": {
                        "id": 13,
                        "group": "core",
                        "label": "City",
                        "alias": "city",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "state": {
                        "id": 14,
                        "group": "core",
                        "label": "State",
                        "alias": "state",
                        "type": "region",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "zipcode": {
                        "id": 15,
                        "group": "core",
                        "label": "Zip Code",
                        "alias": "zipcode",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "country": {
                        "id": 16,
                        "group": "core",
                        "label": "Country",
                        "alias": "country",
                        "type": "country",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "preferred_locale": {
                        "id": 17,
                        "group": "core",
                        "label": "Preferred Locale",
                        "alias": "preferred_locale",
                        "type": "locale",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "timezone": {
                        "id": 18,
                        "group": "core",
                        "label": "mautic.lead.field.timezone",
                        "alias": "timezone",
                        "type": "timezone",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "last_active": {
                        "id": 19,
                        "group": "core",
                        "label": "Date Last Active",
                        "alias": "last_active",
                        "type": "datetime",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "attribution_date": {
                        "id": 20,
                        "group": "core",
                        "label": "Attribution Date",
                        "alias": "attribution_date",
                        "type": "datetime",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "attribution": {
                        "id": 21,
                        "group": "core",
                        "label": "Attribution",
                        "alias": "attribution",
                        "type": "number",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "website": {
                        "id": 22,
                        "group": "core",
                        "label": "Website",
                        "alias": "website",
                        "type": "url",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    }
                },
                "social": {
                    "facebook": {
                        "id": 23,
                        "group": "social",
                        "label": "Facebook",
                        "alias": "facebook",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "foursquare": {
                        "id": 24,
                        "group": "social",
                        "label": "Foursquare",
                        "alias": "foursquare",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "googleplus": {
                        "id": 25,
                        "group": "social",
                        "label": "Google+",
                        "alias": "googleplus",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "instagram": {
                        "id": 26,
                        "group": "social",
                        "label": "Instagram",
                        "alias": "instagram",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "linkedin": {
                        "id": 27,
                        "group": "social",
                        "label": "LinkedIn",
                        "alias": "linkedin",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "skype": {
                        "id": 28,
                        "group": "social",
                        "label": "Skype",
                        "alias": "skype",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "twitter": {
                        "id": 29,
                        "group": "social",
                        "label": "Twitter",
                        "alias": "twitter",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    }
                },
                "personal": [],
                "professional": [],
                "all": {
                    "id": 32,
                    "title": null,
                    "firstname": null,
                    "lastname": null,
                    "company": null,
                    "position": null,
                    "email": "email1@domain.com",
                    "mobile": null,
                    "phone": null,
                    "points": 10,
                    "fax": null,
                    "address1": null,
                    "address2": null,
                    "city": null,
                    "state": null,
                    "zipcode": null,
                    "country": null,
                    "preferred_locale": null,
                    "timezone": null,
                    "last_active": null,
                    "attribution_date": null,
                    "attribution": null,
                    "website": null,
                    "facebook": null,
                    "foursquare": null,
                    "googleplus": null,
                    "instagram": null,
                    "linkedin": null,
                    "skype": null,
                    "twitter": null
                }
            },
            "lastActive": null,
            "owner": null,
            "ipAddresses": [],
            "tags": [],
            "utmtags": null,
            "stage": null,
            "dateIdentified": "2020-07-09T16:14:14+00:00",
            "preferredProfileImage": null,
            "doNotContact": [],
            "frequencyRules": []
        },
        {
            "isPublished": true,
            "dateAdded": "2020-07-09T16:14:14+00:00",
            "dateModified": null,
            "createdBy": 1,
            "createdByUser": "Super Admin",
            "modifiedBy": null,
            "modifiedByUser": null,
            "id": 33,
            "points": 11,
            "color": null,
            "fields": {
                "core": {
                    "title": {
                        "id": 1,
                        "group": "core",
                        "label": "Title",
                        "alias": "title",
                        "type": "lookup",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "firstname": {
                        "id": 2,
                        "group": "core",
                        "label": "First Name",
                        "alias": "firstname",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "lastname": {
                        "id": 3,
                        "group": "core",
                        "label": "Last Name",
                        "alias": "lastname",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "company": {
                        "id": 4,
                        "group": "core",
                        "label": "Primary company",
                        "alias": "company",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "position": {
                        "id": 5,
                        "group": "core",
                        "label": "Position",
                        "alias": "position",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "email": {
                        "id": 6,
                        "group": "core",
                        "label": "Email",
                        "alias": "email",
                        "type": "email",
                        "object": "lead",
                        "value": "email2@domain.com",
                        "normalizedValue": "email2@domain.com"
                    },
                    "mobile": {
                        "id": 7,
                        "group": "core",
                        "label": "Mobile",
                        "alias": "mobile",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "phone": {
                        "id": 8,
                        "group": "core",
                        "label": "Phone",
                        "alias": "phone",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "points": {
                        "id": 9,
                        "group": "core",
                        "label": "Points",
                        "alias": "points",
                        "type": "number",
                        "object": "lead",
                        "value": 11,
                        "normalizedValue": 11
                    },
                    "fax": {
                        "id": 10,
                        "group": "core",
                        "label": "Fax",
                        "alias": "fax",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "address1": {
                        "id": 11,
                        "group": "core",
                        "label": "Address Line 1",
                        "alias": "address1",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "address2": {
                        "id": 12,
                        "group": "core",
                        "label": "Address Line 2",
                        "alias": "address2",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "city": {
                        "id": 13,
                        "group": "core",
                        "label": "City",
                        "alias": "city",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "state": {
                        "id": 14,
                        "group": "core",
                        "label": "State",
                        "alias": "state",
                        "type": "region",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "zipcode": {
                        "id": 15,
                        "group": "core",
                        "label": "Zip Code",
                        "alias": "zipcode",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "country": {
                        "id": 16,
                        "group": "core",
                        "label": "Country",
                        "alias": "country",
                        "type": "country",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "preferred_locale": {
                        "id": 17,
                        "group": "core",
                        "label": "Preferred Locale",
                        "alias": "preferred_locale",
                        "type": "locale",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "timezone": {
                        "id": 18,
                        "group": "core",
                        "label": "mautic.lead.field.timezone",
                        "alias": "timezone",
                        "type": "timezone",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "last_active": {
                        "id": 19,
                        "group": "core",
                        "label": "Date Last Active",
                        "alias": "last_active",
                        "type": "datetime",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "attribution_date": {
                        "id": 20,
                        "group": "core",
                        "label": "Attribution Date",
                        "alias": "attribution_date",
                        "type": "datetime",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "attribution": {
                        "id": 21,
                        "group": "core",
                        "label": "Attribution",
                        "alias": "attribution",
                        "type": "number",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "website": {
                        "id": 22,
                        "group": "core",
                        "label": "Website",
                        "alias": "website",
                        "type": "url",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    }
                },
                "social": {
                    "facebook": {
                        "id": 23,
                        "group": "social",
                        "label": "Facebook",
                        "alias": "facebook",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "foursquare": {
                        "id": 24,
                        "group": "social",
                        "label": "Foursquare",
                        "alias": "foursquare",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "googleplus": {
                        "id": 25,
                        "group": "social",
                        "label": "Google+",
                        "alias": "googleplus",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "instagram": {
                        "id": 26,
                        "group": "social",
                        "label": "Instagram",
                        "alias": "instagram",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "linkedin": {
                        "id": 27,
                        "group": "social",
                        "label": "LinkedIn",
                        "alias": "linkedin",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "skype": {
                        "id": 28,
                        "group": "social",
                        "label": "Skype",
                        "alias": "skype",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "twitter": {
                        "id": 29,
                        "group": "social",
                        "label": "Twitter",
                        "alias": "twitter",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    }
                },
                "personal": [],
                "professional": [],
                "all": {
                    "id": 33,
                    "title": null,
                    "firstname": null,
                    "lastname": null,
                    "company": null,
                    "position": null,
                    "email": "email2@domain.com",
                    "mobile": null,
                    "phone": null,
                    "points": 11,
                    "fax": null,
                    "address1": null,
                    "address2": null,
                    "city": null,
                    "state": null,
                    "zipcode": null,
                    "country": null,
                    "preferred_locale": null,
                    "timezone": null,
                    "last_active": null,
                    "attribution_date": null,
                    "attribution": null,
                    "website": null,
                    "facebook": null,
                    "foursquare": null,
                    "googleplus": null,
                    "instagram": null,
                    "linkedin": null,
                    "skype": null,
                    "twitter": null
                }
            },
            "lastActive": null,
            "owner": null,
            "ipAddresses": [],
            "tags": [],
            "utmtags": null,
            "stage": null,
            "dateIdentified": "2020-07-09T16:14:14+00:00",
            "preferredProfileImage": null,
            "doNotContact": [],
            "frequencyRules": []
        },
        {
            "isPublished": true,
            "dateAdded": "2020-07-09T16:14:14+00:00",
            "dateModified": null,
            "createdBy": 1,
            "createdByUser": "Super Admin",
            "modifiedBy": null,
            "modifiedByUser": null,
            "id": 34,
            "points": 12,
            "color": null,
            "fields": {
                "core": {
                    "title": {
                        "id": 1,
                        "group": "core",
                        "label": "Title",
                        "alias": "title",
                        "type": "lookup",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "firstname": {
                        "id": 2,
                        "group": "core",
                        "label": "First Name",
                        "alias": "firstname",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "lastname": {
                        "id": 3,
                        "group": "core",
                        "label": "Last Name",
                        "alias": "lastname",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "company": {
                        "id": 4,
                        "group": "core",
                        "label": "Primary company",
                        "alias": "company",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "position": {
                        "id": 5,
                        "group": "core",
                        "label": "Position",
                        "alias": "position",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "email": {
                        "id": 6,
                        "group": "core",
                        "label": "Email",
                        "alias": "email",
                        "type": "email",
                        "object": "lead",
                        "value": "email3@domain.com",
                        "normalizedValue": "email3@domain.com"
                    },
                    "mobile": {
                        "id": 7,
                        "group": "core",
                        "label": "Mobile",
                        "alias": "mobile",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "phone": {
                        "id": 8,
                        "group": "core",
                        "label": "Phone",
                        "alias": "phone",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "points": {
                        "id": 9,
                        "group": "core",
                        "label": "Points",
                        "alias": "points",
                        "type": "number",
                        "object": "lead",
                        "value": 12,
                        "normalizedValue": 12
                    },
                    "fax": {
                        "id": 10,
                        "group": "core",
                        "label": "Fax",
                        "alias": "fax",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "address1": {
                        "id": 11,
                        "group": "core",
                        "label": "Address Line 1",
                        "alias": "address1",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "address2": {
                        "id": 12,
                        "group": "core",
                        "label": "Address Line 2",
                        "alias": "address2",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "city": {
                        "id": 13,
                        "group": "core",
                        "label": "City",
                        "alias": "city",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "state": {
                        "id": 14,
                        "group": "core",
                        "label": "State",
                        "alias": "state",
                        "type": "region",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "zipcode": {
                        "id": 15,
                        "group": "core",
                        "label": "Zip Code",
                        "alias": "zipcode",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "country": {
                        "id": 16,
                        "group": "core",
                        "label": "Country",
                        "alias": "country",
                        "type": "country",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "preferred_locale": {
                        "id": 17,
                        "group": "core",
                        "label": "Preferred Locale",
                        "alias": "preferred_locale",
                        "type": "locale",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "timezone": {
                        "id": 18,
                        "group": "core",
                        "label": "mautic.lead.field.timezone",
                        "alias": "timezone",
                        "type": "timezone",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "last_active": {
                        "id": 19,
                        "group": "core",
                        "label": "Date Last Active",
                        "alias": "last_active",
                        "type": "datetime",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "attribution_date": {
                        "id": 20,
                        "group": "core",
                        "label": "Attribution Date",
                        "alias": "attribution_date",
                        "type": "datetime",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "attribution": {
                        "id": 21,
                        "group": "core",
                        "label": "Attribution",
                        "alias": "attribution",
                        "type": "number",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "website": {
                        "id": 22,
                        "group": "core",
                        "label": "Website",
                        "alias": "website",
                        "type": "url",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    }
                },
                "social": {
                    "facebook": {
                        "id": 23,
                        "group": "social",
                        "label": "Facebook",
                        "alias": "facebook",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "foursquare": {
                        "id": 24,
                        "group": "social",
                        "label": "Foursquare",
                        "alias": "foursquare",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "googleplus": {
                        "id": 25,
                        "group": "social",
                        "label": "Google+",
                        "alias": "googleplus",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "instagram": {
                        "id": 26,
                        "group": "social",
                        "label": "Instagram",
                        "alias": "instagram",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "linkedin": {
                        "id": 27,
                        "group": "social",
                        "label": "LinkedIn",
                        "alias": "linkedin",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "skype": {
                        "id": 28,
                        "group": "social",
                        "label": "Skype",
                        "alias": "skype",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "twitter": {
                        "id": 29,
                        "group": "social",
                        "label": "Twitter",
                        "alias": "twitter",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    }
                },
                "personal": [],
                "professional": [],
                "all": {
                    "id": 34,
                    "title": null,
                    "firstname": null,
                    "lastname": null,
                    "company": null,
                    "position": null,
                    "email": "email3@domain.com",
                    "mobile": null,
                    "phone": null,
                    "points": 12,
                    "fax": null,
                    "address1": null,
                    "address2": null,
                    "city": null,
                    "state": null,
                    "zipcode": null,
                    "country": null,
                    "preferred_locale": null,
                    "timezone": null,
                    "last_active": null,
                    "attribution_date": null,
                    "attribution": null,
                    "website": null,
                    "facebook": null,
                    "foursquare": null,
                    "googleplus": null,
                    "instagram": null,
                    "linkedin": null,
                    "skype": null,
                    "twitter": null
                }
            },
            "lastActive": null,
            "owner": null,
            "ipAddresses": [],
            "tags": [],
            "utmtags": null,
            "stage": null,
            "dateIdentified": "2020-07-09T16:14:14+00:00",
            "preferredProfileImage": null,
            "doNotContact": [],
            "frequencyRules": []
        }
    ],
    "statusCodes": [
        201,
        201,
        201
    ]
}
```

An example of request

```http
POST /api/contacts/batch/new HTTP/1.1
Authorization: Basic YWRtaW46cGFzc3dvcmQ=
Content-Type: application/json
User-Agent: PostmanRuntime/7.26.1
Accept: */*
Postman-Token: c48a9f4e-0564-4be6-90fc-fb7a253f2213
Host: localhost:8081
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 245

{
    "key-1": {
        "email": "email1@domain.com",
        "points": 10
    },
    "key-2": {
        "email": "email2@domain.com",
        "points": 11
    },
    "key-3": {
        "email": "email3@domain.com",
        "points": 12
    }
}
```

An example of response

```http
HTTP/1.1 201 Created
Date: Thu, 09 Jul 2020 16:22:46 GMT
Server: Apache/2.4.38 (Debian)
Vary: Authorization
X-Powered-By: PHP/7.1.33
Set-Cookie: f3fdad43054b85e090478a8ee7b8df63=fafd6f13b6b320a855deea2f1253d790; path=/; HttpOnly
Mautic-Version: 2.16.2
Cache-Control: no-cache
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
Transfer-Encoding: chunked
Content-Type: application/json

{
    "contacts": {
        "key-1": {
            "isPublished": true,
            "dateAdded": "2020-07-09T16:22:47+00:00",
            "dateModified": null,
            "createdBy": 1,
            "createdByUser": "Super Admin",
            "modifiedBy": null,
            "modifiedByUser": null,
            "id": 35,
            "points": 10,
            "color": null,
            "fields": {
                "core": {
                    "title": {
                        "id": 1,
                        "group": "core",
                        "label": "Title",
                        "alias": "title",
                        "type": "lookup",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "firstname": {
                        "id": 2,
                        "group": "core",
                        "label": "First Name",
                        "alias": "firstname",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "lastname": {
                        "id": 3,
                        "group": "core",
                        "label": "Last Name",
                        "alias": "lastname",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "company": {
                        "id": 4,
                        "group": "core",
                        "label": "Primary company",
                        "alias": "company",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "position": {
                        "id": 5,
                        "group": "core",
                        "label": "Position",
                        "alias": "position",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "email": {
                        "id": 6,
                        "group": "core",
                        "label": "Email",
                        "alias": "email",
                        "type": "email",
                        "object": "lead",
                        "value": "email1@domain.com",
                        "normalizedValue": "email1@domain.com"
                    },
                    "mobile": {
                        "id": 7,
                        "group": "core",
                        "label": "Mobile",
                        "alias": "mobile",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "phone": {
                        "id": 8,
                        "group": "core",
                        "label": "Phone",
                        "alias": "phone",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "points": {
                        "id": 9,
                        "group": "core",
                        "label": "Points",
                        "alias": "points",
                        "type": "number",
                        "object": "lead",
                        "value": 10,
                        "normalizedValue": 10
                    },
                    "fax": {
                        "id": 10,
                        "group": "core",
                        "label": "Fax",
                        "alias": "fax",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "address1": {
                        "id": 11,
                        "group": "core",
                        "label": "Address Line 1",
                        "alias": "address1",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "address2": {
                        "id": 12,
                        "group": "core",
                        "label": "Address Line 2",
                        "alias": "address2",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "city": {
                        "id": 13,
                        "group": "core",
                        "label": "City",
                        "alias": "city",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "state": {
                        "id": 14,
                        "group": "core",
                        "label": "State",
                        "alias": "state",
                        "type": "region",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "zipcode": {
                        "id": 15,
                        "group": "core",
                        "label": "Zip Code",
                        "alias": "zipcode",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "country": {
                        "id": 16,
                        "group": "core",
                        "label": "Country",
                        "alias": "country",
                        "type": "country",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "preferred_locale": {
                        "id": 17,
                        "group": "core",
                        "label": "Preferred Locale",
                        "alias": "preferred_locale",
                        "type": "locale",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "timezone": {
                        "id": 18,
                        "group": "core",
                        "label": "mautic.lead.field.timezone",
                        "alias": "timezone",
                        "type": "timezone",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "last_active": {
                        "id": 19,
                        "group": "core",
                        "label": "Date Last Active",
                        "alias": "last_active",
                        "type": "datetime",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "attribution_date": {
                        "id": 20,
                        "group": "core",
                        "label": "Attribution Date",
                        "alias": "attribution_date",
                        "type": "datetime",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "attribution": {
                        "id": 21,
                        "group": "core",
                        "label": "Attribution",
                        "alias": "attribution",
                        "type": "number",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "website": {
                        "id": 22,
                        "group": "core",
                        "label": "Website",
                        "alias": "website",
                        "type": "url",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    }
                },
                "social": {
                    "facebook": {
                        "id": 23,
                        "group": "social",
                        "label": "Facebook",
                        "alias": "facebook",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "foursquare": {
                        "id": 24,
                        "group": "social",
                        "label": "Foursquare",
                        "alias": "foursquare",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "googleplus": {
                        "id": 25,
                        "group": "social",
                        "label": "Google+",
                        "alias": "googleplus",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "instagram": {
                        "id": 26,
                        "group": "social",
                        "label": "Instagram",
                        "alias": "instagram",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "linkedin": {
                        "id": 27,
                        "group": "social",
                        "label": "LinkedIn",
                        "alias": "linkedin",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "skype": {
                        "id": 28,
                        "group": "social",
                        "label": "Skype",
                        "alias": "skype",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "twitter": {
                        "id": 29,
                        "group": "social",
                        "label": "Twitter",
                        "alias": "twitter",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    }
                },
                "personal": [],
                "professional": [],
                "all": {
                    "id": 35,
                    "title": null,
                    "firstname": null,
                    "lastname": null,
                    "company": null,
                    "position": null,
                    "email": "email1@domain.com",
                    "mobile": null,
                    "phone": null,
                    "points": 10,
                    "fax": null,
                    "address1": null,
                    "address2": null,
                    "city": null,
                    "state": null,
                    "zipcode": null,
                    "country": null,
                    "preferred_locale": null,
                    "timezone": null,
                    "last_active": null,
                    "attribution_date": null,
                    "attribution": null,
                    "website": null,
                    "facebook": null,
                    "foursquare": null,
                    "googleplus": null,
                    "instagram": null,
                    "linkedin": null,
                    "skype": null,
                    "twitter": null
                }
            },
            "lastActive": null,
            "owner": null,
            "ipAddresses": [],
            "tags": [],
            "utmtags": null,
            "stage": null,
            "dateIdentified": "2020-07-09T16:22:47+00:00",
            "preferredProfileImage": null,
            "doNotContact": [],
            "frequencyRules": []
        },
        "key-2": {
            "isPublished": true,
            "dateAdded": "2020-07-09T16:22:47+00:00",
            "dateModified": null,
            "createdBy": 1,
            "createdByUser": "Super Admin",
            "modifiedBy": null,
            "modifiedByUser": null,
            "id": 36,
            "points": 11,
            "color": null,
            "fields": {
                "core": {
                    "title": {
                        "id": 1,
                        "group": "core",
                        "label": "Title",
                        "alias": "title",
                        "type": "lookup",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "firstname": {
                        "id": 2,
                        "group": "core",
                        "label": "First Name",
                        "alias": "firstname",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "lastname": {
                        "id": 3,
                        "group": "core",
                        "label": "Last Name",
                        "alias": "lastname",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "company": {
                        "id": 4,
                        "group": "core",
                        "label": "Primary company",
                        "alias": "company",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "position": {
                        "id": 5,
                        "group": "core",
                        "label": "Position",
                        "alias": "position",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "email": {
                        "id": 6,
                        "group": "core",
                        "label": "Email",
                        "alias": "email",
                        "type": "email",
                        "object": "lead",
                        "value": "email2@domain.com",
                        "normalizedValue": "email2@domain.com"
                    },
                    "mobile": {
                        "id": 7,
                        "group": "core",
                        "label": "Mobile",
                        "alias": "mobile",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "phone": {
                        "id": 8,
                        "group": "core",
                        "label": "Phone",
                        "alias": "phone",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "points": {
                        "id": 9,
                        "group": "core",
                        "label": "Points",
                        "alias": "points",
                        "type": "number",
                        "object": "lead",
                        "value": 11,
                        "normalizedValue": 11
                    },
                    "fax": {
                        "id": 10,
                        "group": "core",
                        "label": "Fax",
                        "alias": "fax",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "address1": {
                        "id": 11,
                        "group": "core",
                        "label": "Address Line 1",
                        "alias": "address1",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "address2": {
                        "id": 12,
                        "group": "core",
                        "label": "Address Line 2",
                        "alias": "address2",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "city": {
                        "id": 13,
                        "group": "core",
                        "label": "City",
                        "alias": "city",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "state": {
                        "id": 14,
                        "group": "core",
                        "label": "State",
                        "alias": "state",
                        "type": "region",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "zipcode": {
                        "id": 15,
                        "group": "core",
                        "label": "Zip Code",
                        "alias": "zipcode",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "country": {
                        "id": 16,
                        "group": "core",
                        "label": "Country",
                        "alias": "country",
                        "type": "country",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "preferred_locale": {
                        "id": 17,
                        "group": "core",
                        "label": "Preferred Locale",
                        "alias": "preferred_locale",
                        "type": "locale",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "timezone": {
                        "id": 18,
                        "group": "core",
                        "label": "mautic.lead.field.timezone",
                        "alias": "timezone",
                        "type": "timezone",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "last_active": {
                        "id": 19,
                        "group": "core",
                        "label": "Date Last Active",
                        "alias": "last_active",
                        "type": "datetime",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "attribution_date": {
                        "id": 20,
                        "group": "core",
                        "label": "Attribution Date",
                        "alias": "attribution_date",
                        "type": "datetime",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "attribution": {
                        "id": 21,
                        "group": "core",
                        "label": "Attribution",
                        "alias": "attribution",
                        "type": "number",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "website": {
                        "id": 22,
                        "group": "core",
                        "label": "Website",
                        "alias": "website",
                        "type": "url",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    }
                },
                "social": {
                    "facebook": {
                        "id": 23,
                        "group": "social",
                        "label": "Facebook",
                        "alias": "facebook",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "foursquare": {
                        "id": 24,
                        "group": "social",
                        "label": "Foursquare",
                        "alias": "foursquare",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "googleplus": {
                        "id": 25,
                        "group": "social",
                        "label": "Google+",
                        "alias": "googleplus",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "instagram": {
                        "id": 26,
                        "group": "social",
                        "label": "Instagram",
                        "alias": "instagram",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "linkedin": {
                        "id": 27,
                        "group": "social",
                        "label": "LinkedIn",
                        "alias": "linkedin",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "skype": {
                        "id": 28,
                        "group": "social",
                        "label": "Skype",
                        "alias": "skype",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "twitter": {
                        "id": 29,
                        "group": "social",
                        "label": "Twitter",
                        "alias": "twitter",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    }
                },
                "personal": [],
                "professional": [],
                "all": {
                    "id": 36,
                    "title": null,
                    "firstname": null,
                    "lastname": null,
                    "company": null,
                    "position": null,
                    "email": "email2@domain.com",
                    "mobile": null,
                    "phone": null,
                    "points": 11,
                    "fax": null,
                    "address1": null,
                    "address2": null,
                    "city": null,
                    "state": null,
                    "zipcode": null,
                    "country": null,
                    "preferred_locale": null,
                    "timezone": null,
                    "last_active": null,
                    "attribution_date": null,
                    "attribution": null,
                    "website": null,
                    "facebook": null,
                    "foursquare": null,
                    "googleplus": null,
                    "instagram": null,
                    "linkedin": null,
                    "skype": null,
                    "twitter": null
                }
            },
            "lastActive": null,
            "owner": null,
            "ipAddresses": [],
            "tags": [],
            "utmtags": null,
            "stage": null,
            "dateIdentified": "2020-07-09T16:22:47+00:00",
            "preferredProfileImage": null,
            "doNotContact": [],
            "frequencyRules": []
        },
        "key-3": {
            "isPublished": true,
            "dateAdded": "2020-07-09T16:22:47+00:00",
            "dateModified": null,
            "createdBy": 1,
            "createdByUser": "Super Admin",
            "modifiedBy": null,
            "modifiedByUser": null,
            "id": 37,
            "points": 12,
            "color": null,
            "fields": {
                "core": {
                    "title": {
                        "id": 1,
                        "group": "core",
                        "label": "Title",
                        "alias": "title",
                        "type": "lookup",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "firstname": {
                        "id": 2,
                        "group": "core",
                        "label": "First Name",
                        "alias": "firstname",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "lastname": {
                        "id": 3,
                        "group": "core",
                        "label": "Last Name",
                        "alias": "lastname",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "company": {
                        "id": 4,
                        "group": "core",
                        "label": "Primary company",
                        "alias": "company",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "position": {
                        "id": 5,
                        "group": "core",
                        "label": "Position",
                        "alias": "position",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "email": {
                        "id": 6,
                        "group": "core",
                        "label": "Email",
                        "alias": "email",
                        "type": "email",
                        "object": "lead",
                        "value": "email3@domain.com",
                        "normalizedValue": "email3@domain.com"
                    },
                    "mobile": {
                        "id": 7,
                        "group": "core",
                        "label": "Mobile",
                        "alias": "mobile",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "phone": {
                        "id": 8,
                        "group": "core",
                        "label": "Phone",
                        "alias": "phone",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "points": {
                        "id": 9,
                        "group": "core",
                        "label": "Points",
                        "alias": "points",
                        "type": "number",
                        "object": "lead",
                        "value": 12,
                        "normalizedValue": 12
                    },
                    "fax": {
                        "id": 10,
                        "group": "core",
                        "label": "Fax",
                        "alias": "fax",
                        "type": "tel",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "address1": {
                        "id": 11,
                        "group": "core",
                        "label": "Address Line 1",
                        "alias": "address1",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "address2": {
                        "id": 12,
                        "group": "core",
                        "label": "Address Line 2",
                        "alias": "address2",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "city": {
                        "id": 13,
                        "group": "core",
                        "label": "City",
                        "alias": "city",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "state": {
                        "id": 14,
                        "group": "core",
                        "label": "State",
                        "alias": "state",
                        "type": "region",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "zipcode": {
                        "id": 15,
                        "group": "core",
                        "label": "Zip Code",
                        "alias": "zipcode",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "country": {
                        "id": 16,
                        "group": "core",
                        "label": "Country",
                        "alias": "country",
                        "type": "country",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "preferred_locale": {
                        "id": 17,
                        "group": "core",
                        "label": "Preferred Locale",
                        "alias": "preferred_locale",
                        "type": "locale",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "timezone": {
                        "id": 18,
                        "group": "core",
                        "label": "mautic.lead.field.timezone",
                        "alias": "timezone",
                        "type": "timezone",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "last_active": {
                        "id": 19,
                        "group": "core",
                        "label": "Date Last Active",
                        "alias": "last_active",
                        "type": "datetime",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "attribution_date": {
                        "id": 20,
                        "group": "core",
                        "label": "Attribution Date",
                        "alias": "attribution_date",
                        "type": "datetime",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "attribution": {
                        "id": 21,
                        "group": "core",
                        "label": "Attribution",
                        "alias": "attribution",
                        "type": "number",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "website": {
                        "id": 22,
                        "group": "core",
                        "label": "Website",
                        "alias": "website",
                        "type": "url",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    }
                },
                "social": {
                    "facebook": {
                        "id": 23,
                        "group": "social",
                        "label": "Facebook",
                        "alias": "facebook",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "foursquare": {
                        "id": 24,
                        "group": "social",
                        "label": "Foursquare",
                        "alias": "foursquare",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "googleplus": {
                        "id": 25,
                        "group": "social",
                        "label": "Google+",
                        "alias": "googleplus",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "instagram": {
                        "id": 26,
                        "group": "social",
                        "label": "Instagram",
                        "alias": "instagram",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "linkedin": {
                        "id": 27,
                        "group": "social",
                        "label": "LinkedIn",
                        "alias": "linkedin",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "skype": {
                        "id": 28,
                        "group": "social",
                        "label": "Skype",
                        "alias": "skype",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    },
                    "twitter": {
                        "id": 29,
                        "group": "social",
                        "label": "Twitter",
                        "alias": "twitter",
                        "type": "text",
                        "object": "lead",
                        "value": null,
                        "normalizedValue": null
                    }
                },
                "personal": [],
                "professional": [],
                "all": {
                    "id": 37,
                    "title": null,
                    "firstname": null,
                    "lastname": null,
                    "company": null,
                    "position": null,
                    "email": "email3@domain.com",
                    "mobile": null,
                    "phone": null,
                    "points": 12,
                    "fax": null,
                    "address1": null,
                    "address2": null,
                    "city": null,
                    "state": null,
                    "zipcode": null,
                    "country": null,
                    "preferred_locale": null,
                    "timezone": null,
                    "last_active": null,
                    "attribution_date": null,
                    "attribution": null,
                    "website": null,
                    "facebook": null,
                    "foursquare": null,
                    "googleplus": null,
                    "instagram": null,
                    "linkedin": null,
                    "skype": null,
                    "twitter": null
                }
            },
            "lastActive": null,
            "owner": null,
            "ipAddresses": [],
            "tags": [],
            "utmtags": null,
            "stage": null,
            "dateIdentified": "2020-07-09T16:22:47+00:00",
            "preferredProfileImage": null,
            "doNotContact": [],
            "frequencyRules": []
        }
    },
    "statusCodes": {
        "key-1": 201,
        "key-2": 201,
        "key-3": 201
    }
}
```