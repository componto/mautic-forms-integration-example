## IMPORTANT NOTE:

Client cookies will always need to be sent to backend. This example of implementation uses the `escopecz/mautic-form-submit` package that handles cookies automatically. Please read more about the Mautic Forms here: 
https://bitbucket.org/componto/mautic-forms-integration-example/src/mautic-forms-integration-example/docs/mobile-integration-with-the-mautic-forms.md

## Mautic Forms integration

This guide will show to you how to create an integration with Mautic forms. The integration is based on form submission via AJAX request and a PHP library to simplify mautic forms submission from third party systems.

![form-page.png](images/form-page.png)

The form uses the default HTML5 validation of inputs at frontend and represents two custom fields (Email and Points). After submission you will get an alert with cookies.

![submitted-form.png](images/submitted-form.png)

```
Cookies: {"id":1485,"sid":"icmlhapliznqrzna1o2a153","device_id":"icmlhapliznqrzna1o2a153"}
```

### Creating the Mautic Form

We need to create a form to allow collect contact data. It has to have the fields named the same as your form.

![created-mautic-form-with-two-custom-fields.png](images/created-mautic-form-with-two-custom-fields.png)

### Project preparation

Let�s clone an larave project to develop our custom integration with the Mautic Forms.
```
git clone https://github.com/laravel/laravel.git
```

To create own branch
```
git checkout tags/v7.12.0 -b mautic-forms-integration-example
```

to install bootstrap 4 in your laravel 7 project then install following laravel ui composer package to get command:
```
composer require laravel/ui
```

After successfully install above package then we are ready to install bootstrap 4 with our application.
```
php artisan ui bootstrap
```

We will be using the `escopecz/mautic-form-submit` package that handles cookies automatically.

To install PHP library for submitting Mautic Form
```
composer require escopecz/mautic-form-submit
```

You also need to install npm and run it. so let's run both command:

Install NPM
```
npm install
```

Run NPM
```
npm run watch
```

Now we can work with bootstrap 4 application.

### Creating a configuration file

We need to have some place where we can easily manage mautic configurations and for that we gonna to create a configuration file with the `mautic` name in the `config` folder.

```php
<?php

return [
    'mautic_url' => 'http://mautic2160',
    'forms' => [
        'from_4' => 4
    ]
];
```

### Creating a service provider for Mautic Forms

The Artisan CLI can generate a new provider via the `make:provider` command:

```
php artisan make:provider MauticFormServiceProvider
```

Within the register method, we will bind `Mautic::class` into the service container. 

```php
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Escopecz\MauticFormSubmit\Mautic;

class MauticFormServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Mautic::class, function ($app) {
            // instantiate the Mautic object with the base URL where the Mautic runs
            return new Mautic(config('mautic.mautic_url'));
        });
    }
}
```

We need to declare the provider at `config/app.php` file

```php
'providers' => [

    //...

    App\Providers\MauticFormServiceProvider::class,
],
```

### Creating a controller to handle AJAX request

You may generate an controller by using the `make:controller` Artisan command:
```
php artisan make:controller FormController
```

First, we will inject the `Mautic` service that is able to submit form data to Mautic and the `Illuminate\Http\Request` object, which holds the POST data and other data about the request.

The `submit` method will process the inputs that our AJAX call sent and spit back the json response that we created. You can see this in your browser's console after you submit your form.

That method takes the form data and sends them to Mautic as well by using injected the `Mautic` service.

You can access any of these attributes as part of your data object. Just use the . to get to a specific item.

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Escopecz\MauticFormSubmit\Mautic;

class FormController extends Controller
{
    /** 
     * @var Mautic
     */ 
    protected $mautic;

    public function __construct(Mautic $mautic)
    {
        $this->mautic = $mautic;
    }

    public function submit(Request $request)
    {
        // create a new instance of the Form object with the form ID 4
        $form = $this->mautic->getForm(config('mautic.forms.from_4'));

        $result = $form->submit([
            'email' => $request->input('email'),
            'points' => (int) $request->input('points'),
        ]);

        $new_cookie = $result['new_cookie'];

        return response()->json([
            'success' => true,
            'data' => [
                'cookies' => [
                    'id' => $new_cookie['mtc_id'],
                    'sid' => $new_cookie['mtc_sid'],
                    'device_id' => $new_cookie['mautic_device_id'],
                ]
            ]
        ]);
    }
}
```

### Displaying the Submission Form

We will be using Bootstrap to build out our views. It's just easier since all their classes are already pre-built and we can create our view quickly.

We need to create the `forms.blade.php` template at `resources/views/forms.blade.php` with the following boilerplate bootstrap markup:

```html
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Forms | Mautic</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
   
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <meta name="csrf-token" content="{{ csrf_token() }}">

    </head>
    <body>
        <div class="container form-container">

            <div class="row">
                <form class="form">
                    <div class="text-center mb-4">
                        <h1 class="h3 mb-3 font-weight-normal">Form</h1>
                    </div>

                    <div class="form-label-group">
                        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email" required="" autofocus="">
                        <label for="inputEmail">Email</label>
                    </div>

                    <div class="form-label-group">
                        <input type="number" name="points" id="inputPoints" class="form-control" placeholder="Points" required="">
                        <label for="inputPoints">Points</label>
                    </div>
                    
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
                </form>
            </div>

            <div class="row justify-content-center">
                <div class="alert alert-dismissible fade" role="alert">
                    <div class="alert-message"></div>

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

        </div>

        <script>
            (function(w,d,t,u,n,a,m){w['MauticTrackingObject']=n;
                w[n]=w[n]||function(){(w[n].q=w[n].q||[]).push(arguments)},a=d.createElement(t),
                m=d.getElementsByTagName(t)[0];a.async=1;a.src=u;m.parentNode.insertBefore(a,m)
            })(window,document,'script','http://192.168.1.172:8081/index_dev.php/mtc.js','mt');

            mt('send', 'pageview');
        </script>
    </body>
</html>
```

### Submitting the Form

With the form in place, we are ready to handle sending form data on the server.

```js
(function($) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function Form (container)
    {
        this.$el = $('.' + container + ' .form');
        this.$alert = $('.' + container + ' .alert');

        this.status = '';
    }

    Form.prototype.init = function ()
    {
        this.$el.on("submit", this.onCloseAlert.bind(this));
        this.$el.on('submit', this.onSubmit.bind(this));
        this.$alert.on("close.bs.alert", this.onCloseAlert.bind(this));
    }

    Form.prototype.onCloseAlert = function()
    {
        this.$alert.removeClass('show');
        this.$alert.removeClass('alert-' + this.status);
        
        return false;
    }

    Form.prototype.showAlert = function(status, message)
    {
        this.$alert.find('.alert-message').html(message);
        this.$alert.addClass('alert-' + status);
        this.$alert.addClass('show');

        this.status = status;
    }

    Form.prototype.onSubmit = function(e)
    {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: "/forms/submit",
            data: this.$el.serialize(),
            success: this.onSuccess.bind(this),
            error: this.onError.bind(this),
        });
    }

    Form.prototype.onSuccess = function(response)
    {
        if (response.success) {
            this.showAlert('success', '<strong>Cookies:</strong> ' + JSON.stringify(response.data.cookies));
        } else {
            this.showAlert('danger', '<strong>Error</strong> Oops, something went wrong...');
            console.log(response);
            
            return;
        }

        if (response.data.cookies) {
            MauticJS.setTrackedContact(response.data.cookies);
        }

        this.$el.trigger('reset');
    }

    Form.prototype.onError = function(response)
    {
        this.showAlert('danger', '<strong>Error</strong> Oops, something went wrong...');
        console.log(response);
        return;
    }


    $( document ).ready(function() {

        let form = new Form('form-container');

        form.init();
    });

})(jQuery);
```

Then we need to import our `form.js` file into the main js file.

```js
require('./bootstrap');
require('./form');
```

To compile all assets run `npm run dev` and `npm run production` commands.

### Routing

We need to update the main project route and also define a new route that will handle our form submission. We can add new routes to our application in the `routes/web.php` file.

The main page of our application will be render a submission from. To handle AJAX request of a from submittion we will define the `/forms/submit` path and will attach it to our controller.

To create a new route, we will use a route closure and a dedicated controller class.

```php
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('forms');
});

Route::post('/forms/submit', 'FormController@submit');
```

Now the application is ready to run and use.
