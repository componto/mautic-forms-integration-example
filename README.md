## Example of the Mautic Forms integration 

Please read the guide of how to integrate Mautic Forms with WEB application [here](./docs/mautic-forms-integration-example.md).

Please read the guide of how to integrate Mautic Forms with mobile application [here](./docs/mobile-integration-with-the-mautic-forms.md).

![headers-tab.png](docs/images/headers-tab.png)

![response-preview-tab.png](docs/images/response-preview-tab.png)

![cookies-tab.png](docs/images/cookies-tab.png)

![contact-page.png](docs/images/contact-page.png)
