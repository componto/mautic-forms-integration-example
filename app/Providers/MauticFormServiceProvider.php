<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Escopecz\MauticFormSubmit\Mautic;

class MauticFormServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Mautic::class, function ($app) {
            // instantiate the Mautic object with the base URL where the Mautic runs
            return new Mautic(config('mautic.mautic_url'));
        });
    }
}
