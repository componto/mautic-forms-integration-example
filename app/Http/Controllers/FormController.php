<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Escopecz\MauticFormSubmit\Mautic;

class FormController extends Controller
{
    /** 
     * @var Mautic
     */ 
    protected $mautic;

    public function __construct(Mautic $mautic)
    {
        $this->mautic = $mautic;
    }

    public function submit(Request $request)
    {
        // create a new instance of the Form object with the form ID 4
        $form = $this->mautic->getForm(config('mautic.forms.from_4'));

        $result = $form->submit([
            'email' => $request->input('email'),
            'points' => (int) $request->input('points'),
        ]);

        $new_cookie = $result['new_cookie'];

        return response()->json([
            'success' => true,
            'data' => [
                'cookies' => [
                    'id' => $new_cookie['mtc_id'],
                    'sid' => $new_cookie['mtc_sid'],
                    'device_id' => $new_cookie['mautic_device_id'],
                ]
            ]
        ]);
    }
}
