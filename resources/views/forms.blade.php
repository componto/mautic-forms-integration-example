<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Forms | Mautic</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
   
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <meta name="csrf-token" content="{{ csrf_token() }}">

    </head>
    <body>
        <div class="container form-container">

            <div class="row">
                <form class="form">
                    <div class="text-center mb-4">
                        <h1 class="h3 mb-3 font-weight-normal">Form</h1>
                    </div>

                    <div class="form-label-group">
                        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email" required="" autofocus="">
                        <label for="inputEmail">Email</label>
                    </div>

                    <div class="form-label-group">
                        <input type="number" name="points" id="inputPoints" class="form-control" placeholder="Points" required="">
                        <label for="inputPoints">Points</label>
                    </div>
                    
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
                </form>
            </div>

            <div class="row justify-content-center">
                <div class="alert alert-dismissible fade" role="alert">
                    <div class="alert-message"></div>

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

        </div>

        <script>
            (function(w,d,t,u,n,a,m){w['MauticTrackingObject']=n;
                w[n]=w[n]||function(){(w[n].q=w[n].q||[]).push(arguments)},a=d.createElement(t),
                m=d.getElementsByTagName(t)[0];a.async=1;a.src=u;m.parentNode.insertBefore(a,m)
            })(window,document,'script','http://192.168.1.172:8081/index_dev.php/mtc.js','mt');

            mt('send', 'pageview');
        </script>
    </body>
</html>
