(function($) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function Form (container)
    {
        this.$el = $('.' + container + ' .form');
        this.$alert = $('.' + container + ' .alert');

        this.status = '';
    }

    Form.prototype.init = function ()
    {
        this.$el.on("submit", this.onCloseAlert.bind(this));
        this.$el.on('submit', this.onSubmit.bind(this));
        this.$alert.on("close.bs.alert", this.onCloseAlert.bind(this));
    }

    Form.prototype.onCloseAlert = function()
    {
        this.$alert.removeClass('show');
        this.$alert.removeClass('alert-' + this.status);
        
        return false;
    }

    Form.prototype.showAlert = function(status, message)
    {
        this.$alert.find('.alert-message').html(message);
        this.$alert.addClass('alert-' + status);
        this.$alert.addClass('show');

        this.status = status;
    }

    Form.prototype.onSubmit = function(e)
    {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: "/forms/submit",
            data: this.$el.serialize(),
            success: this.onSuccess.bind(this),
            error: this.onError.bind(this),
        });
    }

    Form.prototype.onSuccess = function(response)
    {
        if (response.success) {
            this.showAlert('success', '<strong>Cookies:</strong> ' + JSON.stringify(response.data.cookies));
        } else {
            this.showAlert('danger', '<strong>Error</strong> Oops, something went wrong...');
            console.log(response);
            
            return;
        }

        if (response.data.cookies) {
            MauticJS.setTrackedContact(response.data.cookies);
        }

        this.$el.trigger('reset');
    }

    Form.prototype.onError = function(response)
    {
        this.showAlert('danger', '<strong>Error</strong> Oops, something went wrong...');
        console.log(response);
        return;
    }


    $( document ).ready(function() {

        let form = new Form('form-container');

        form.init();
    });

})(jQuery);